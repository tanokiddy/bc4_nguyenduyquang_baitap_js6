//BT1
document.getElementById("btnTimSoN").onclick = function () {
  //input
  //ouput: number
  var n = 0;
  //progress
  for (var i = 1, sum = 0; sum < 10000; i++) {
    sum += i;
  }
  n = i - 1;
  document.getElementById("ketQua1").innerHTML = n;
};
//BT2
document.getElementById("btnTinhTongNX").onclick = function () {
  //input: number
  var soThu1 = document.getElementById("nhapSo_1").value * 1;
  var soThu2 = document.getElementById("nhapSo_2").value * 1;
  //output: number
  var tinhTong = 0;
  //progress:
  for (var soDem = 1; soDem <= soThu2; soDem++) {
    tinhTong += Math.pow(soThu1, soDem);
  }
  document.getElementById("ketQua2").innerHTML = tinhTong;
};
//BT3
document.getElementById("btnTinhGiaiThua").onclick = function () {
  var n = document.getElementById("nhapSo").value * 1;
  var giaiThua = 1;
  var giaTriBanDau = 1;
  while (giaTriBanDau <= n) {
    giaiThua *= giaTriBanDau;
    giaTriBanDau++;
  }
  document.getElementById("ketQua3").innerHTML = "Kết quả: " + giaiThua;
};

//BT4
document.getElementById("btnInTheDiv").onclick = function () {
  //input: 10 number
  //b1
  var dem = 1;
  //output: string
  var hienThiDiv = "";
  //b2
  while (dem <= 10) {
    var div = "";
    if (dem % 2 == 0) {
      div = '<div class="alert alert-danger">Thẻ div chẵn</div>';
    } else {
      div = '<div class="alert alert-primary">Thẻ div lẻ</div>';
    }
    hienThiDiv += div;
    dem++;
  }
  document.getElementById("ketQua4").innerHTML = hienThiDiv;
};

//BT5
document.getElementById("btnTinhSNT").onclick = function () {
  var nhapSo = document.getElementById("nhapSo_5").value * 1;
  var chuoiSo = "";
  for (iSo = 2; iSo <= nhapSo; iSo++) {
    var checkSNT = true;
    for (var i = 2; i <= Math.sqrt(iSo); i++) {
      if (iSo % i === 0) {
        checkSNT = false;
        break;
      }
    }
    if (checkSNT) {
      chuoiSo += iSo + " ";
    }
  }

  document.getElementById("ketQua5").innerHTML = chuoiSo;
};
